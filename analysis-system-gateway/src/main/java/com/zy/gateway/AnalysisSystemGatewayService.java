package com.zy.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 *  EnableDiscoveryClient 服务发现
 * 本服务启动后会自动注册进eureka服务中
 * scanBasePackages 自定义模块包描注
 */
@SpringBootApplication(
        scanBasePackages = {
                "com.zy.gateway","com.zy.oauth","com.zy.hasm"
        }

)
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients
public class AnalysisSystemGatewayService {
    public static void main(String[] args) {
        SpringApplication.run(AnalysisSystemGatewayService.class,args);
    }
}
