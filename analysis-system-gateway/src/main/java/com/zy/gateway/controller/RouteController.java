package com.zy.gateway.controller;

import com.zy.core.utils.JwtUtil;
import com.zy.hasm.entity.AdminAnalysisStation;
import com.zy.hasm.entity.AdminAnalysisUser;
import com.zy.hasm.service.AdminAnalysisUserService;
import com.zy.hasm.service.AnalysisStationService;
import com.zy.oauth.dto.LoginUser;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 路由跳转控制器
 * @version 0.0.1
 * @since 2023/4/13
 */
@Controller
public class RouteController {

    @Autowired
    private JwtUtil jwtUtil;

    private static  LoginUser user;

    @Autowired
    private AdminAnalysisUserService adminAnalysisUserService;

    @Autowired
    private AnalysisStationService analysisStationService;

    @GetMapping({"/","/index"})
    public String toIndex(Model model){
        if(user!= null){
            model.addAttribute("user",user);
        }

        return "index";
    }

    @GetMapping({"/toLogin"})
    public String toLogin(){
        user = null;
        return "user/login";
    }

    @GetMapping({"/toRegister"})
    public String toRegister(){
        return "user/register";
    }

    @GetMapping({"/admin/index/{username}"})
    public String toAdminIndex(@PathVariable("username") String username, Model model){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        return "admin/index";
    }

    @ApiModelProperty("详情")
    @GetMapping("/info")
    public String info(@RequestParam("token") String token, Model model){
        user = jwtUtil.parseToken(token, LoginUser.class);
        model.addAttribute("user",user);
        return "index";
    }


    @GetMapping("/logout")
    public String logout(Model model){
        user = null;
        return "redirect:/";
    }


    @GetMapping({"/admin/user-list"})
    public String toUserList(Model model){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        return "admin/user/userList";
    }

    @GetMapping("/admin/user-add")
    public String toAddPage(Model model){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        return "admin/user/userAdd";
    }

    @GetMapping("/admin/user-edit/{userId}")
    public String toEditPage(@PathVariable("userId") String userId,Model model){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        AdminAnalysisUser updateUser = adminAnalysisUserService.selectUserById(userId);
        model.addAttribute("updateUser",updateUser);
        return "admin/user/userEdit";
    }

    @GetMapping({"/admin/station-list"})
    public String toStationList(Model model){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        return "admin/station/stationList";
    }

    @GetMapping("/admin/station-add")
    public String toStationAddPage(Model model){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        return "admin/station/stationAdd";
    }

    @GetMapping("/admin/station-edit/{stationId}")
    public String toStationEditPage(@PathVariable("stationId") String stationId, Model model){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        AdminAnalysisStation station = analysisStationService.selectOne(stationId);
        model.addAttribute("station",station);
        return "admin/station/stationEdit";
    }

    @GetMapping("/admin/to-ratio-page")
    public String toRatioPage(Model model){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        return "count/ratioPage";
    }

    @GetMapping("/admin/to-travel-probability-f1-page")
    public String travelProbabilityF1Page(Model model){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        return "count/travelProbabilityF1Page";
    }

    @GetMapping("/admin/self-edit/{username}")
    public String toSelfEdit(Model model, @PathVariable String username){
        model.addAttribute("user",user);
        String role = user.getPermissions().get(0);
        model.addAttribute("role",role);
        AdminAnalysisUser updateUser = adminAnalysisUserService.selectUserByName(username);
        model.addAttribute("updateUser",updateUser);
        return "admin/user/selfEdit";
    }


}
