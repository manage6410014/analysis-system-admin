package com.zy.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @version 0.0.1
 * @since 2023/4/13
 */
@Configuration
@EnableSwagger2
@EnableOpenApi
public class SwaggerConfiguration {

    // 页面信息
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("大站快车客流分析系统Swagger在线文档")
                .version("1.0-SNAPSHOT")
                .license("项目版本：1.0-SNAPSHOT")
                .license("作者：王钲尧")
                .build();
    }



    /**
     * 配置认证模块服务Docket实例
     * @return
     */
    @Bean
    public Docket getOauthDocket(){
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zy.oauth.controller"))
                .build()
                .pathMapping("/oauth/")
                .groupName("analysis-system-oauth-service");
    }

    /**
     * 配置认证模块服务Docket实例
     * @return
     */
    @Bean
    public Docket getAdminDocket(){
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zy.hasm.controller"))
                .build()
                .pathMapping("/admin/")
                .groupName("analysis-system-manage-service");
    }

}
