package com.zy.core.pagehelper;

import java.util.List;


@FunctionalInterface
public interface Select<E> {
    List<E> doSelect();
}
