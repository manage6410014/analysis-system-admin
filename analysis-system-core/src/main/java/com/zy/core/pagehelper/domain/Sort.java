package com.zy.core.pagehelper.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.*;


@JsonDeserialize(
        using = SortDeserializer.class
)
public class Sort implements Iterable<Sort.Order>, Serializable {
    private static final long serialVersionUID = 5737186511678863905L;
    public static final Direction DEFAULT_DIRECTION;
    private final List<Order> orders;

    public Sort(Order... orders) {
        this(Arrays.asList(orders));
    }

    public Sort(List<Order> orders) {
        if (null != orders && !orders.isEmpty()) {
            this.orders = orders;
        } else {
            throw new IllegalArgumentException("You have to provide at least one sort property to sort by!");
        }
    }

    public Sort(String... properties) {
        this(DEFAULT_DIRECTION, properties);
    }

    public Sort(Direction direction, String... properties) {
        this(direction, (List)(properties == null ? new ArrayList() : Arrays.asList(properties)));
    }

    public Sort(Direction direction, List<String> properties) {
        if (properties != null && !properties.isEmpty()) {
            this.orders = new ArrayList(properties.size());
            Iterator var3 = properties.iterator();

            while(var3.hasNext()) {
                String property = (String)var3.next();
                this.orders.add(new Order(direction, property));
            }

        } else {
            throw new IllegalArgumentException("You have to provide at least one property to sort by!");
        }
    }

    public Sort and(Sort sort) {
        if (sort == null) {
            return this;
        } else {
            ArrayList<Order> these = new ArrayList(this.orders);
            Iterator var3 = sort.iterator();

            while(var3.hasNext()) {
                Order order = (Order)var3.next();
                these.add(order);
            }

            return new Sort(these);
        }
    }

    public Order getOrderFor(String property) {
        Iterator var2 = this.iterator();

        Order order;
        do {
            if (!var2.hasNext()) {
                return null;
            }

            order = (Order)var2.next();
        } while(!order.getProperty().equals(property));

        return order;
    }

    @Override
    public Iterator<Order> iterator() {
        return this.orders.iterator();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (!(obj instanceof Sort)) {
            return false;
        } else {
            Sort that = (Sort)obj;
            return this.orders.equals(that.orders);
        }
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    @Override
    public String toString() {
        return StringUtils.collectionToCommaDelimitedString(this.orders);
    }

    static {
        DEFAULT_DIRECTION = Direction.ASC;
    }

    public static class Order implements Serializable {
        private static final long serialVersionUID = 1522511010900108987L;
        private static final boolean DEFAULT_IGNORE_CASE = false;
        private final Direction direction;
        private String property;
        private boolean propertyChanged;
        private final boolean ignoreCase;
        private final NullHandling nullHandling;

        public Order(Direction direction, String property) {
            this(direction, property, false, (NullHandling)null);
        }

        public Order(Direction direction, String property, NullHandling nullHandlingHint) {
            this(direction, property, false, nullHandlingHint);
        }

        public Order(String property) {
            this(Sort.DEFAULT_DIRECTION, property);
        }

        private Order(Direction direction, String property, boolean ignoreCase, NullHandling nullHandling) {
            if (!StringUtils.hasText(property)) {
                throw new IllegalArgumentException("Property must not null or empty!");
            } else {
                this.direction = direction == null ? Sort.DEFAULT_DIRECTION : direction;
                this.property = property;
                this.ignoreCase = ignoreCase;
                this.nullHandling = nullHandling == null ? NullHandling.NATIVE : nullHandling;
            }
        }

        public Direction getDirection() {
            return this.direction;
        }

        public String getProperty() {
            return this.property;
        }

        public boolean isPropertyChanged() {
            return this.propertyChanged;
        }

        public void setPropertyChanged(boolean propertyChanged) {
            this.propertyChanged = propertyChanged;
        }

        public void setProperty(String property) {
            this.setPropertyChanged(true);
            this.property = property;
        }

        public boolean isAscending() {
            return this.direction.isAscending();
        }

        public boolean isDescending() {
            return this.direction.isDescending();
        }

        public boolean isIgnoreCase() {
            return this.ignoreCase;
        }

        public Order with(Direction direction) {
            return new Order(direction, this.property, this.ignoreCase, this.nullHandling);
        }

        public Order with(NullHandling nullHandling) {
            return new Order(this.direction, this.property, this.ignoreCase, nullHandling);
        }

        public Order withProperty(String property) {
            return new Order(this.direction, property, this.ignoreCase, this.nullHandling);
        }

        public Sort withProperties(String... properties) {
            return new Sort(this.direction, properties);
        }

        public Order ignoreCase() {
            return new Order(this.direction, this.property, true, this.nullHandling);
        }

        public Order nullsFirst() {
            return this.with(NullHandling.NULLS_FIRST);
        }

        public Order nullsLast() {
            return this.with(NullHandling.NULLS_LAST);
        }

        public Order nullsNative() {
            return this.with(NullHandling.NATIVE);
        }

        public NullHandling getNullHandling() {
            return this.nullHandling;
        }

        @Override
        public int hashCode() {

            return super.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            } else if (!(obj instanceof Order)) {
                return false;
            } else {
                Order that = (Order)obj;
                return this.direction.equals(that.direction) && this.property.equals(that.property) && this.ignoreCase == that.ignoreCase && this.nullHandling.equals(that.nullHandling);
            }
        }
        @Override
        public String toString() {
            String result = String.format("%s: %s", this.property, this.direction);
            if (!NullHandling.NATIVE.equals(this.nullHandling)) {
                result = result + ", " + this.nullHandling;
            }

            if (this.ignoreCase) {
                result = result + ", ignoring case";
            }

            return result;
        }
    }

    public static enum NullHandling {
        NATIVE,
        NULLS_FIRST,
        NULLS_LAST;

        private NullHandling() {
        }
    }

    public static enum Direction {
        ASC,
        DESC;

        private Direction() {
        }

        public boolean isAscending() {
            return this.equals(ASC);
        }

        public boolean isDescending() {
            return this.equals(DESC);
        }

        public static Direction fromString(String value) {
            try {
                return valueOf(value.toUpperCase(Locale.US));
            } catch (Exception var2) {
                throw new IllegalArgumentException(String.format("Invalid value '%s' for orders given! Has to be either 'desc' or 'asc' (case insensitive).", value), var2);
            }
        }

        public static Direction fromStringOrNull(String value) {
            try {
                return fromString(value);
            } catch (IllegalArgumentException var2) {
                return null;
            }
        }
    }
}
