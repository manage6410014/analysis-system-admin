package com.zy.core.pagehelper.page;

import com.zy.core.domain.PageInfo;
import com.zy.core.pagehelper.Select;
import com.zy.core.pagehelper.domain.PageRequest;
import com.zy.core.pagehelper.domain.Sort;



public abstract class PageMethod {
    protected static final ThreadLocal<PageInfo> LOCAL_PAGE = new ThreadLocal();
    protected static final ThreadLocal<Sort> LOCAL_SORT = new ThreadLocal();

    protected PageMethod() {
    }

    public static PageInfo getLocalPage() {
        return (PageInfo)LOCAL_PAGE.get();
    }

    protected static void setLocalPage(PageInfo info) {
        LOCAL_PAGE.set(info);
    }

    public static void clearPage() {
        LOCAL_PAGE.remove();
    }

    protected static void setLocalSort(Sort sort) {
        LOCAL_SORT.set(sort);
    }

    public static Sort getLocalSort() {
        return (Sort)LOCAL_SORT.get();
    }

    public static void clearSort() {
        LOCAL_SORT.remove();
    }

    public static long count(Select select) {
        PageInfo info = startPage(0, 2147483647, true);
        select.doSelect();
        return info.getTotal();
    }

    public static PageInfo startPage(int pageNum, int pageSize) {
        return startPage(pageNum, pageSize, true);
    }

    public static PageInfo startPage(PageRequest pageRequest) {
        PageInfo page = new PageInfo(pageRequest.getPage(), pageRequest.getSize(), true);
        if (pageRequest.getTotalElements() > 0L) {
            page.setTotal(pageRequest.getTotalElements());
        }

        setLocalPage(page);
        return page;
    }

    public static PageInfo startPage(int pageNum, int pageSize, boolean count) {
        PageInfo page = new PageInfo(pageNum, pageSize, count);
        setLocalPage(page);
        return page;
    }

    public static PageInfo startPageAndSort(PageRequest pageRequest) {
        return startPageAndSort(pageRequest, true);
    }

    public static PageInfo startPageAndSort(PageRequest pageRequest, boolean count) {
        PageInfo page = new PageInfo(pageRequest.getPage(), pageRequest.getSize(), count);
        if (pageRequest.getTotalElements() > 0L) {
            page.setTotal(pageRequest.getTotalElements());
        }

        setLocalPage(page);
        setLocalSort(pageRequest.getSort());
        return page;
    }

    public static void startSort(Sort sort) {
        setLocalSort(sort);
    }
}
