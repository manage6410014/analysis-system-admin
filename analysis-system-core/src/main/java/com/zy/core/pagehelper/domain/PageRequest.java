package com.zy.core.pagehelper.domain;

import com.zy.core.pagehelper.domain.Sort.*;


public class PageRequest {
    private int page;
    private int size;
    private Sort sort;
    private long totalElements;

    public PageRequest() {
    }

    public PageRequest(int page, int size) {
        this(page, size, (Sort)null);
    }

    public PageRequest(int page, int size, Sort sort) {
        this.page = page;
        this.size = size;
        this.sort = sort;
    }

    public PageRequest(int page, int size, Direction direction, String... properties) {
        this(page, size, new Sort(direction, properties));
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return this.size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Sort getSort() {
        return this.sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public long getTotalElements() {
        return this.totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

}
