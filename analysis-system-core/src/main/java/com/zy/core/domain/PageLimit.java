package com.zy.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/4/16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageLimit implements Serializable {

    private Integer page;
    private Integer limit;
    public Long getStart(){
//        1后加L 转换成Long  否则会报类型错误  Start开始页面
        return (page-1L)*limit;
    }
}
