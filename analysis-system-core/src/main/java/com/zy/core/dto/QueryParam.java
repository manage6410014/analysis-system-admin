package com.zy.core.dto;


import com.zy.core.domain.PageLimit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;



@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryParam extends PageLimit {
    private String last;
    private String first;
    private String name;
    private String role;
    private String title;
    private String startDate;
    private String endDate;
    private String user;
    private String code;
}
