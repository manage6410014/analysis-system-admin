package com.zy.core.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @Order 注解执行优先级.value 越小 优先级越高
 */
@Component
@Aspect
@Order
public class LogRequiredAspect {

    private static final Logger logger = LoggerFactory.getLogger(LogRequiredAspect.class);



    /**
     * 设置使用这个注解的方法所在组成切面
     */
    @Pointcut("@annotation(com.zy.core.annotation.LogRequired)")
    public void pointcut() {
    }

    /**
     * doBefore()在切面前执行
     */
    @Before("pointcut()")
    public void doBefore(JoinPoint joinPoint) {
        logger.debug("===================doBefore==============");
        logger.debug("开始执行" + joinPoint.getSignature().getName() + "任务，参数为：" + Arrays.toString(joinPoint.getArgs()));
    }

    @After("pointcut()")
    public void doAfter(JoinPoint joinPoint){
        logger.debug("===================doAfter==============");
        logger.debug(""+joinPoint.getSignature().getName()+"方法运行后。。。@After");
    }

    @AfterReturning(value = "pointcut()", returning = "proceed")
    public Object afterReturning(JoinPoint joinPoint, Object proceed){
        logger.debug("===================AfterReturning==============");
        logger.debug("====================执行结果=============={}",proceed);
        return proceed;
    }

    /**
     * 若切面方法运行代码后有错误抛出，则进入此处进行操作
     */
    @AfterThrowing(value = "pointcut()" , throwing = "exception")
    public void afterThrowing(Exception exception){
        exception.printStackTrace();
    }

}
