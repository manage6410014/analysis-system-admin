package com.zy.core.utils;

import lombok.Data;

/**
 * 公共的结果类
 * @author 33029
 */
@Data
@SuppressWarnings("unchecked")
public class Result<T> {


    private Integer code;
    private String message;
    private T data;

    private Long count;


    private Result(){}
    private Result(Integer code,String message,T data,Long count){
        this.code=code;
        this.message=message;
        this.data=data;
        this.count=count;
    }

    public static Result<Object> success(String message){
     return  new Result(0,message,null,null);
    }

    public static Result success(Object data, String message, Long count){
        return  new Result(0,message,data,count);
    }

    public static Result<Object> fail(){
        return  new Result(-1,"fail",null,null);
    }

    public static Result<Object> fail(String message){
        return  new Result(-1,message,null,null);
    }

    public static Result<Object> success(Object data, Long count) {
        return new Result<>(0,"success",data,count);
    }
}
