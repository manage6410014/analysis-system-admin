package com.zy.core.utils;


import com.zy.core.formula.NumberContext;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
public class FunctionUtil {


    /**
     * 自然常量 e 计算
     * @return
     */
    public static String exponential() {
        BigDecimal e = BigDecimal.ONE;
        int n =1;
        BigDecimal temp = BigDecimal.ONE;
        while (temp.compareTo(BigDecimal.valueOf(1e-6))>0){
            e = e.add(temp);
            n++;
            temp  =  divide(BigDecimal.ONE ,fact(n));
        }
        log.debug("e=============={}",e);
        return e.toString();
    }

    /**
     * 计算阶乘 x!
     * @return x!
     */
    public static BigDecimal fact(int i) {
        BigDecimal sum = BigDecimal.ONE;
        for (int m=1;m<=i;m++){
            sum = sum.multiply(BigDecimal.valueOf(m));

        }
        log.debug(i+ "!'" +"={}",sum);
        return sum;
    }

    /**
     * x^n
     */
    public static BigDecimal power(double x, int n) {
        double a = x;
        double res = 1;
//   2147483648   超出int类型的最大值
        long y =n;
 //  指数小于0,底数取倒数、指数转为正数
        if(n<0){
            y=-y;
            x=1/x;
        }

        while (y>0){
    //   判断是否为奇数
            if(y%2 == 1){
                res = res * x;
            }
            x=x*x;
            y=y>>1;
        }
       BigDecimal temp = divide(BigDecimal.valueOf(res),BigDecimal.ONE);
        log.debug(a+"^("+n+")="+temp);
        return temp;
    }

    /**
     * x/y
     */
    public static BigDecimal divide(BigDecimal x ,BigDecimal y){
        NumberContext nc = NumberContext.defaultContext();
        BigDecimal divide = x.divide(y, nc.getScale(), nc.getRoundingMode());
        log.debug(x+"/"+y+"===={}",divide);
        return  divide;
    }
}
