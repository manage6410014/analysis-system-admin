package com.zy.core.utils;



public class RequestUtil {
    public static final String HTTP = "http";
    public static final String HTTPS = "https";

    public RequestUtil() {
    }

    public static String getRequestDomain() {
        return null;
    }

    public static String getProtocol() {
        String baseUrl = getRequestDomain();
        if (baseUrl.startsWith("http://")) {
            return "http";
        } else if (baseUrl.startsWith("https://")) {
            return "https";
        } else {
            throw new IllegalArgumentException("base-url should start with 'http://' or 'https://'");
        }
    }
}
