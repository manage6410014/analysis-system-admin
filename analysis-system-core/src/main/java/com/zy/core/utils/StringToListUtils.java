package com.zy.core.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@Slf4j
public class StringToListUtils {
    /**
     *
     * @param str
     * str -> strList
     * @return strList
     */
    public static List<String> stringToList(String str){
        List<String> resList = Collections.emptyList();
        if(StringUtils.isNotBlank(str)){
            List<String> list = new ArrayList<>();
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                // 出现 逗号、顿号、空格、回车 就切割
                if (str.charAt(i) == ',' || str.charAt(i) == '、' || str.charAt(i) == ' '|| str.charAt(i) == '\n') {
                    list.add(res.toString());
                    res = new StringBuilder();
                    continue;
                }

                if (i == str.length() - 1) {
                    res.append(str.charAt(i));
                    list.add(res.toString());
                }
                res.append(str.charAt(i));
            }
            // 去重、去空
            resList = list.stream().distinct().filter((x) -> !StringUtils.isBlank(x)).collect(Collectors.toList());
        }

        return resList;
    }

}
