package com.zy.core.formula;

import java.math.RoundingMode;


public class NumberContext {
    private final int scale;
    private final RoundingMode roundingMode;

    public NumberContext(int scale, RoundingMode roundingMode) {
        this.scale = scale;
        this.roundingMode = roundingMode;
    }

    public int getScale() {
        return this.scale;
    }

    public RoundingMode getRoundingMode() {
        return this.roundingMode;
    }

    public static NumberContext defaultContext() {
        return new NumberContext(8, RoundingMode.HALF_UP);
    }
}

