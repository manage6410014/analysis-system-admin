package com.zy.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LogRequired {
    /**
     * 操作名称
     */
    String operateName() default "";

    /**
     * 请求参数DTO名称、用于映射参数Key
     */
    String requestBodyName() default "";
}
