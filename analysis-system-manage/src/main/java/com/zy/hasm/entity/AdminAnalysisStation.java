package com.zy.hasm.entity;

import java.beans.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

import com.alibaba.fastjson2.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;


/**
 * 站点信息表(AnalysisStation)实体类
 *
 * @author wangzhengyao
 * @since 2023-04-16 21:07:07
 */

@Data
@ApiModel("站点信息表")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@TableName("zy_analysis_station")
public class AdminAnalysisStation implements Serializable {
    private static final long serialVersionUID = 826468145227443524L;

    public static final String FIELD_STATION_ID = "stationId";
    public static final String FIELD_STATION_NAME = "stationName";
    public static final String FIELD_STATION_CODE = "stationCode";
    public static final String FIELD_PEOPLE_GET_ON = "peopleGetOn";
    public static final String FIELD_PEOPLE_GET_OFF = "peopleGetOff";
    public static final String FIELD_RATIO = "ratio";

    @ApiModelProperty("主键")
    @TableId(value = "station_id",type = IdType.AUTO)
    private String stationId;

    @ApiModelProperty(value = "站点名称")
    private String stationName;

    @ApiModelProperty(value = "站点编码")
    private String stationCode;

    @ApiModelProperty(value = "上车人数")
    private Integer peopleGetOn;

    @ApiModelProperty(value = "下车人数")
    private Integer peopleGetOff;

    @ApiModelProperty(value = "不均衡系数")
    private BigDecimal ratio;

    @ApiModelProperty(value = "OD出行概率-f1")
    private BigDecimal travelProbabilityF1;

    @ApiModelProperty(value = "OD出行客流人数-f1-页面展示")
    @TableField(exist = false)
    private BigDecimal f1People;

    @ApiModelProperty(value = "OD出行概率-f1-页面展示")
    @TableField(exist = false)
    private BigDecimal f1Value;


    @ApiModelProperty(value = "集散量")
    @TableField(exist = false)
    private Integer distribution;


}

