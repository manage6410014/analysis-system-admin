package com.zy.hasm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * SpringBootApplication 主启动类
 * EnableEurekaClient 本服务启动后会自动注册进eureka服务中
 */
@SpringBootApplication(
        scanBasePackages = {
                "com.zy.core",
                "com.zy.hasm"
        }
)
@MapperScan("com.zy.hasm.mapper")
@EnableEurekaClient
public class AnalysisSystemManageService {
    public static void main(String[] args) {
        SpringApplication.run(AnalysisSystemManageService.class,args);
    }
}
