package com.zy.hasm.service.impl;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zy.core.dto.QueryParam;
import com.zy.core.utils.StringToListUtils;
import com.zy.hasm.entity.AdminAnalysisUser;
import com.zy.hasm.mapper.AdminAnalysisUserMapper;
import com.zy.hasm.service.AdminAnalysisUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @version 0.0.1
 * @since 2023/4/16
 */
@Service
public class AdminAnalysisUserServiceImpl implements AdminAnalysisUserService {

    @Autowired
    private AdminAnalysisUserMapper adminAnalysisUserMapper;


    @Override
    public List<AdminAnalysisUser> getUserList(QueryParam queryParam) {
        LambdaQueryWrapper<AdminAnalysisUser> queryWrapper = new LambdaQueryWrapper<>();
        return  adminAnalysisUserMapper.selectList(queryWrapper);
    }

    @Override
    public Boolean checkUsername(String username) {
        LambdaQueryWrapper<AdminAnalysisUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AdminAnalysisUser::getUserName,username);
        List<AdminAnalysisUser> userList = adminAnalysisUserMapper.selectList(queryWrapper);
        return CollectionUtils.isEmpty(userList);
    }

    @Override
    @Transactional
    public void addUser(AdminAnalysisUser adminAnalysisUser) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        // 默认密码 123
        adminAnalysisUser.setPassWord(bCryptPasswordEncoder.encode("123"));

        adminAnalysisUserMapper.insert(adminAnalysisUser);

        AdminAnalysisUser analysisUser = adminAnalysisUserMapper.selectUserByName(adminAnalysisUser.getUserName());
        // 初始化角色为普通用户
        adminAnalysisUserMapper.initUserRole(analysisUser.getUserId());
    }

    @Override
    public void delUserByIds(String ids) {
        Map<String, Object> map = (Map<String, Object>) JSON.parse(ids);
        Object delIds = map.get("ids");
        List<String> strings = StringToListUtils.stringToList(delIds.toString());
        adminAnalysisUserMapper.deleteBatchIds(strings);
    }

    @Override
    public AdminAnalysisUser selectOne(String userId) {
        LambdaQueryWrapper<AdminAnalysisUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AdminAnalysisUser::getUserId,userId);
        return adminAnalysisUserMapper.selectOne(queryWrapper);
    }

    @Override
    public void editUser(AdminAnalysisUser adminAnalysisUser) {

        String adminFlag = adminAnalysisUser.getAdminFlag();
        if(adminFlag.equals("Y")){
            adminAnalysisUserMapper.updateRoleAdmin(adminAnalysisUser.getUserId());
        }
        if(adminFlag.equals("N")){
            adminAnalysisUserMapper.updateRoleUser(adminAnalysisUser.getUserId());
        }
        adminAnalysisUserMapper.updateById(adminAnalysisUser);
    }



    @Override
    public List<AdminAnalysisUser> selectList(QueryParam queryParam) {
        return adminAnalysisUserMapper.selectUserList(queryParam);
    }

    @Override
    public AdminAnalysisUser selectUserById(String userId) {
        return adminAnalysisUserMapper.selectUserById(userId);
    }

    @Override
    public AdminAnalysisUser selectUserByName(String username) {
        return adminAnalysisUserMapper.selectUserByName(username);
    }

    @Override
    public void selfEdit(AdminAnalysisUser adminAnalysisUser) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        adminAnalysisUser.setPassWord(bCryptPasswordEncoder.encode(adminAnalysisUser.getPassWord()));
        adminAnalysisUserMapper.updateById(adminAnalysisUser);
    }
}
