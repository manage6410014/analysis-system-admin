package com.zy.hasm.service;

import com.zy.core.dto.QueryParam;
import com.zy.hasm.entity.AdminAnalysisStation;

import java.util.List;

/**
 * 站点信息表(AnalysisStation)应用服务
 *
 * @author wangzhengyao
 * @since 2023-04-16 21:07:09
 */

public interface AnalysisStationService {

    List<AdminAnalysisStation> selectList(QueryParam queryParam);


    void addStation(AdminAnalysisStation adminAnalysisStation);


    void editStation(AdminAnalysisStation adminAnalysisStation);

    void delStationByIds(String ids);

    Boolean checkStationName(String stationName);

    AdminAnalysisStation selectOne(String stationId);
}
