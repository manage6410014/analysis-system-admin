package com.zy.hasm.service;

import com.zy.core.dto.QueryParam;
import com.zy.hasm.entity.AdminAnalysisUser;

import java.util.List;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/4/16
 */
public interface AdminAnalysisUserService {

    List<AdminAnalysisUser> getUserList(QueryParam queryParam);

    Boolean checkUsername(String username);

    void addUser(AdminAnalysisUser adminAnalysisUser);

    void delUserByIds(String ids);

    AdminAnalysisUser selectOne(String userId);


    void editUser(AdminAnalysisUser adminAnalysisUser);

    List<AdminAnalysisUser> selectList(QueryParam queryParam);

    AdminAnalysisUser selectUserById(String userId);


    AdminAnalysisUser selectUserByName(String username);

    void selfEdit(AdminAnalysisUser adminAnalysisUser);

}
