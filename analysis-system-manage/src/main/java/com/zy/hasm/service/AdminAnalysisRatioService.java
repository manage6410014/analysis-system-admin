package com.zy.hasm.service;

import com.zy.hasm.entity.AdminAnalysisStation;

import java.util.List;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/4/22
 */
public interface AdminAnalysisRatioService {

    /**
     * 更新不均衡系数
     */
    List<AdminAnalysisStation>  ratioRefresh();
    /**
     * 更新OD出行概率
     */
    List<AdminAnalysisStation> travelProbabilityF1Refresh();

}
