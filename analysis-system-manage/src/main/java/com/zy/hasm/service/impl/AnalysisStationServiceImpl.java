package com.zy.hasm.service.impl;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zy.core.dto.QueryParam;
import com.zy.core.utils.StringToListUtils;
import com.zy.hasm.entity.AdminAnalysisStation;
import com.zy.hasm.entity.AdminAnalysisUser;
import com.zy.hasm.mapper.AnalysisStationMapper;
import com.zy.hasm.service.AnalysisStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * 站点信息表(AnalysisStation)应用服务
 *
 * @author wangzhengyao
 * @since 2023-04-16 21:07:09
 */
@Service
public class AnalysisStationServiceImpl implements AnalysisStationService {

    @Autowired
    private AnalysisStationMapper analysisStationMapper;

    @Override
    public List<AdminAnalysisStation> selectList(QueryParam queryParam) {
        return analysisStationMapper.selectStationList(queryParam);
    }

    @Override
    public void addStation(AdminAnalysisStation adminAnalysisStation) {
        analysisStationMapper.insert(adminAnalysisStation);
    }

    @Override
    public void editStation(AdminAnalysisStation adminAnalysisStation) {
        analysisStationMapper.updateById(adminAnalysisStation);
    }

    @Override
    public void delStationByIds(String ids) {
        Map<String, Object> map = (Map<String, Object>) JSON.parse(ids);
        Object delIds = map.get("ids");
        List<String> strings = StringToListUtils.stringToList(delIds.toString());
        analysisStationMapper.deleteBatchIds(strings);

    }

    @Override
    public Boolean checkStationName(String stationName) {
        LambdaQueryWrapper<AdminAnalysisStation> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AdminAnalysisStation::getStationName,stationName);
        List<AdminAnalysisStation> stationList = analysisStationMapper.selectList(queryWrapper);
        return CollectionUtils.isEmpty(stationList);
    }

    @Override
    public AdminAnalysisStation selectOne(String stationId) {
        return analysisStationMapper.selectStationById(stationId);
    }
}
