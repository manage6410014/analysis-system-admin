package com.zy.hasm.service.impl;

import com.zy.core.formula.NumberContext;
import com.zy.core.utils.FunctionUtil;
import com.zy.hasm.entity.AdminAnalysisStation;
import com.zy.hasm.mapper.AnalysisStationMapper;
import com.zy.hasm.service.AdminAnalysisRatioService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @version 0.0.1
 * @since 2023/4/22
 */
@Slf4j
@Service
public class AdminAnalysisRatioServiceImpl implements AdminAnalysisRatioService {

    @Autowired
    private AnalysisStationMapper analysisStationMapper;

    @Override
    public List<AdminAnalysisStation> ratioRefresh () {
        List<AdminAnalysisStation> stationList = analysisStationMapper.selectList(null);
        // 计算集散量
        Map<String, Integer> stationMap = stationList.stream().map(x -> {
            AdminAnalysisStation adminAnalysisStation = new AdminAnalysisStation();
            adminAnalysisStation.setDistribution(x.getPeopleGetOn() + x.getPeopleGetOff());
            adminAnalysisStation.setStationId(x.getStationId());
            return adminAnalysisStation;
        }).collect(Collectors.toMap(AdminAnalysisStation::getStationId, AdminAnalysisStation::getDistribution));

        // 总站数量 n 26
        Integer count = stationMap.keySet().size();

        //总集散量 4218
        Integer countDistribution = stationMap.values().stream().reduce(Integer::sum).orElse(0);

        stationList.forEach(station->{
            // vi
            Integer distribution = stationMap.get(station.getStationId());
            // n * v1
            int numerator = count * distribution;
            BigDecimal vi = BigDecimal.valueOf(numerator);
            BigDecimal vn = BigDecimal.valueOf(countDistribution);
            BigDecimal divide = FunctionUtil.divide(vi, vn);
            station.setRatio(divide);
            analysisStationMapper.updateById(station);
        });
        return stationList;

    }

    @Override
    public List<AdminAnalysisStation> travelProbabilityF1Refresh() {
        long startTime = System.currentTimeMillis();

        String e = FunctionUtil.exponential();
        int λ = 8;

        // e^(-λ)
        BigDecimal eλ = FunctionUtil.power(Double.parseDouble(e),-λ);

        List<AdminAnalysisStation> stationList = analysisStationMapper.selectList(null);

        for (int index = 0; index < stationList.size(); index ++) {
            // λ^(j-i)
            BigDecimal power = FunctionUtil.power(λ, index+1);
            // e^(-λ) * λ^(j-i)
            BigDecimal multiply = eλ.multiply(power);
            // (j-i)!
            BigDecimal fact = FunctionUtil.fact(index+1);
            // [e^(-λ) * λ^(j-i)]/ (j-i)!
            BigDecimal divide = FunctionUtil.divide(multiply, fact);
            // 赋值
            AdminAnalysisStation station = stationList.get(index);
            station.setTravelProbabilityF1(divide);
            //客流数据： 第i个站点的上客量* Pij
            Integer peopleGetOn = station.getPeopleGetOn();

            // 避免科学计数法、页面展示结果*100
            station.setF1Value(divide.multiply(new BigDecimal(100)));


            // 计算结果下取整
            BigDecimal f1People = divide.multiply(BigDecimal.valueOf(peopleGetOn)).divide(BigDecimal.ONE, 0,
                    BigDecimal.ROUND_CEILING);
            station.setF1People(f1People);

            analysisStationMapper.updateById(station);
        }

        long endTime = System.currentTimeMillis();

        log.debug("耗时=========={}",(endTime-startTime)/1000);

        return stationList;
    }
}
