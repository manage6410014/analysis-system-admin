package com.zy.hasm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.mapping.ResultSetType;

import java.util.Map;

/**
 * @author yunjiang.chen@leading-finance.com
 * @since 2023/5/6
 */
@Mapper
public interface RemoteMapper {
    @Options(
            resultSetType = ResultSetType.FORWARD_ONLY,
            fetchSize = -2147483648
    )
    @ResultType(Map.class)
    Cursor<Map<String, Object>> selectDs(@Param("sql") String sql);
}
