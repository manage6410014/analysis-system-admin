package com.zy.hasm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.core.dto.QueryParam;
import com.zy.hasm.entity.AdminAnalysisUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Primary;

import java.util.List;
import java.util.Map;


@Mapper
public interface AdminAnalysisUserMapper extends BaseMapper<AdminAnalysisUser> {

    @Primary
    List<AdminAnalysisUser> selectUserList(QueryParam queryParam);

    AdminAnalysisUser selectUserById(@Param("userId") String userId);



    void updateRoleAdmin(@Param("userId")Integer userId);


    void updateRoleUser(@Param("userId") Integer userId);

    void initUserRole(@Param("userId")Integer userId);

    AdminAnalysisUser selectUserByName(@Param("userName")String userName);
}
