package com.zy.hasm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.core.dto.QueryParam;
import com.zy.hasm.entity.AdminAnalysisStation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 站点信息表(AnalysisStation)应用服务
 *
 * @author wangzhengyao
 * @since 2023-04-16 21:07:09
 */
@Mapper
public interface AnalysisStationMapper extends BaseMapper<AdminAnalysisStation> {
    /**
     * 基础查询
     *
     * @param queryParam 查询条件
     * @return 返回值
     */
    List<AdminAnalysisStation> selectStationList(QueryParam queryParam);

    AdminAnalysisStation selectStationById(@Param("stationId") String stationId);


}

