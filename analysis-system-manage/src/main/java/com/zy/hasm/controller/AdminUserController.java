package com.zy.hasm.controller;

import com.zy.core.dto.QueryParam;
import com.zy.core.utils.Result;
import com.zy.core.utils.ResultBean;
import com.zy.hasm.entity.AdminAnalysisUser;
import com.zy.hasm.service.AdminAnalysisUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(
        tags = "AdminUserController"
)
@RestController
@RequestMapping("/user")
public class AdminUserController {

    @Autowired
    private AdminAnalysisUserService adminAnalysisUserService;



    @GetMapping("/list")
    public Result<Object> userList(QueryParam queryParam){
        List<AdminAnalysisUser> adminAnalysisUsers = adminAnalysisUserService.selectList(queryParam);
        return Result.success(adminAnalysisUsers, (long) adminAnalysisUsers.size());
    }

    @PostMapping("/addUser")
    public Result<Object> userAdd(AdminAnalysisUser adminAnalysisUser){
        try {
            adminAnalysisUserService.addUser(adminAnalysisUser);
            return Result.success("新增成功");
        }catch (Exception e){
            return Result.fail("邮箱已被占用");
        }
    }

    @PostMapping("/editUser")
    public Result<Object> editUser(AdminAnalysisUser adminAnalysisUser){
        try {
            adminAnalysisUserService.editUser(adminAnalysisUser);
            return Result.success("修改成功");
        }catch (Exception e){
            return Result.fail("修改失败");
        }
    }

    @GetMapping({"/checkUsername/{username}"})
    public ResultBean<Boolean> checkUsername(@PathVariable("username") String username){
        return new ResultBean<>(adminAnalysisUserService.checkUsername(username));
    }

    @PostMapping ("/delUser")
    public Result<Object> delUser(@RequestBody String ids){
        adminAnalysisUserService.delUserByIds(ids);
        return Result.success("删除成功");
    }

    @PostMapping("/selfEdit")
    @ApiModelProperty("用户信息修改")
    public Result<Object> selfEdit(AdminAnalysisUser adminAnalysisUser){
        try {
            adminAnalysisUserService.selfEdit(adminAnalysisUser);
            return Result.success("修改成功");
        }catch (Exception e){
            e.printStackTrace();
            return Result.fail("修改失败");
        }
    }
}
