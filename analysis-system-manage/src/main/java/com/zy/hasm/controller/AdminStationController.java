package com.zy.hasm.controller;

import com.zy.core.dto.QueryParam;
import com.zy.core.utils.Result;
import com.zy.core.utils.ResultBean;
import com.zy.hasm.entity.AdminAnalysisStation;
import com.zy.hasm.service.AdminAnalysisRatioService;
import com.zy.hasm.service.AnalysisStationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(
        tags = "AdminStationController"
)
@RestController
@RequestMapping("/station")
public class AdminStationController {

    @Autowired
    private AnalysisStationService analysisStationService;

    @Autowired
    private AdminAnalysisRatioService adminAnalysisRatioService;



    @GetMapping("/list")
    public Result<Object> userList(QueryParam queryParam){
        List<AdminAnalysisStation> adminAnalysisStationList = analysisStationService.selectList(queryParam);
        return Result.success(adminAnalysisStationList, (long) adminAnalysisStationList.size());
    }

    @PostMapping("/addStation")
    public Result<Object> addStation(AdminAnalysisStation adminAnalysisStation){
        try {
            analysisStationService.addStation(adminAnalysisStation);
            return Result.success("新增成功");
        }catch (Exception e){
            return Result.fail("站名重复");
        }
    }

    @PostMapping("/editStation")
    public Result<Object> editStation(AdminAnalysisStation adminAnalysisStation){
        try {
            analysisStationService.editStation(adminAnalysisStation);
            return Result.success("修改成功");
        }catch (Exception e){
            return Result.fail("修改失败");
        }
    }


    @PostMapping ("/delStation")
    public Result<Object> delStation(@RequestBody String ids){
        analysisStationService.delStationByIds(ids);
        return Result.success("删除成功");
    }

    @GetMapping({"/checkStationName/{stationName}"})
    public ResultBean<Boolean> checkStationName(@PathVariable("stationName") String stationName){
        return new ResultBean<>(analysisStationService.checkStationName(stationName));
    }

    @ApiModelProperty("刷新新不均衡系数")
    @PostMapping({"/ratio-refresh"})
    public List<AdminAnalysisStation> ratioRefresh(){
        return adminAnalysisRatioService.ratioRefresh();
    }

    @ApiModelProperty("更新OD出行概率-f1")
    @PostMapping({"/travel-probability-f1-refresh"})
    public List<AdminAnalysisStation> travelProbabilityF1Refresh(){
        return adminAnalysisRatioService.travelProbabilityF1Refresh();
    }
}
