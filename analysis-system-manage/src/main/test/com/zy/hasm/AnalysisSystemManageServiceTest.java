package com.zy.hasm;

import com.zy.hasm.mapper.RemoteMapper;
import org.apache.ibatis.cursor.Cursor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author yunjiang.chen@leading-finance.com
 * @since 2023/5/6
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AnalysisSystemManageServiceTest {

    @Autowired
    private RemoteMapper remoteMapper;

    @Test
    @Transactional(readOnly = true)
    public void test(){




//        String s = this.getString();
        Integer count = this.getCount();
        System.err.println(count);

    }

    private Integer getCount() {
        Integer count = null;
        StringBuffer stringBuffer = new StringBuffer("SELECT COUNT(0) as count FROM " +
                "zy_analysis_system" +
                ".zy_analysis_station where 1=2 ");
        Cursor<Map<String, Object>> mapCursor = remoteMapper.selectDs(stringBuffer.toString());
        Iterator<Map<String, Object>> iterator = mapCursor.iterator();
        if(iterator.hasNext()){
            Map<String, Object> map = iterator.next();
             count = Integer.valueOf(map.get("count").toString());
            System.err.println(count);

        }
        return count;
    }


    public String getString() {
        StringBuffer stringBuffer = new StringBuffer("SELECT station_id as stationId, station_name as stationName FROM " +
                "zy_analysis_system" +
                ".zy_analysis_station ");
        Cursor<Map<String, Object>> mapCursor = remoteMapper.selectDs(stringBuffer.toString());
        final String[] stationName1 = {""};
        mapCursor.forEach(map->{
            System.err.println("map:"+map);
            System.err.println("1");
            System.err.println("2");
            System.err.println("map:"+map.get("stationId"));
            String stationId = map.get("stationId").toString();
            String stationName = "";
            if(stationId .equals("109") ){
                stationName = map.get("stationName").toString();
                stationName1[0] = map.get("stationName").toString();
            }
        });
        return stationName1[0];
    }
}
