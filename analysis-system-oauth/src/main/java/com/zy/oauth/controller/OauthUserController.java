package com.zy.oauth.controller;


import com.zy.core.annotation.LogRequired;
import com.zy.core.utils.Results;
import com.zy.oauth.dto.LoginUser;
import com.zy.oauth.service.OauthAnalysisUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author zy
 * @since 2023-04-13
 */
@Api(
        tags = "OauthUserController"
)
@RestController
@RequestMapping("/user")
public class OauthUserController {

    @Autowired
    private OauthAnalysisUserService oauthAnalysisUserService;


    @ApiModelProperty("登录接口")
    @LogRequired
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginUser loginUser){
      return Results.success(oauthAnalysisUserService.login(loginUser));
    }

    @ApiModelProperty("注册接口")
    @LogRequired
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody LoginUser loginUser){
        return Results.success(oauthAnalysisUserService.register(loginUser));
    }


}

