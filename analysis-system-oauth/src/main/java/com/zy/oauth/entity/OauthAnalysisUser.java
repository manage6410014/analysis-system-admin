package com.zy.oauth.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author zy
 * @since 2023-04-13
 */
@Data
@TableName(value = "zy_analysis_user")
public class OauthAnalysisUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 用户登录名
     */
    private String userName;

    /**
     * 密码
     */
    private String passWord;

    /**
     * 性别，0：男，1：女，2：保密
     */
    private Long gender;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 电话号码
     */
    private String phone;

    /**
     * Y:是，N 不是
     */
    private String adminFlag;

}
