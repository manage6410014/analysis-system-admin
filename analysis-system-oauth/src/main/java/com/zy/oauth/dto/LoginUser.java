package com.zy.oauth.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/4/13
 */
@Data
public class LoginUser {

    @ApiModelProperty(value = "用户登录名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "电话号码")
    private String phone;
    @ApiModelProperty(value = "角色&&权限")
    private List<String> permissions;
}
