package com.zy.oauth.dto;

import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yunjiang.chen
 * @version 0.0.1
 * @since 2023/4/13
 */
@NoArgsConstructor
public class CustomerUserDetails implements UserDetails {



    LoginUser loginUser;
    List<String> permissions;



    public CustomerUserDetails(LoginUser loginUser, List<String> permissions) {
        this.loginUser = loginUser;
        this.permissions = permissions;
    }

    /**
     * 权限集合:包括了角色信息以及可访问权限信息
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return permissions.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }



    @Override
    public String getPassword() {
        return loginUser.getPassword();
    }

    @Override
    public String getUsername() {
        return loginUser.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public LoginUser getLoginUser() {
        return loginUser;
    }

    public List<String> getPermissions() {
        return permissions;
    }
}
