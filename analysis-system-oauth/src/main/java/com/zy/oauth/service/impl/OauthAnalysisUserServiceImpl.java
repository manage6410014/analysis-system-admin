package com.zy.oauth.service.impl;


import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zy.core.utils.JwtUtil;
import com.zy.oauth.config.CustomerSecurityConfig;
import com.zy.oauth.dto.CustomerUserDetails;
import com.zy.oauth.dto.LoginUser;
import com.zy.oauth.entity.OauthAnalysisUser;
import com.zy.oauth.mapper.OauthAnalysisUserMapper;
import com.zy.oauth.service.OauthAnalysisUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author zy
 * @since 2023-04-13
 */
@Service
public class OauthAnalysisUserServiceImpl implements OauthAnalysisUserService {

    @Autowired
    private  AuthenticationManager authenticationManager;

    @Autowired
    private OauthAnalysisUserMapper oauthAnalysisUserMapper;

    @Autowired
    private CustomerSecurityConfig customerSecurityConfig;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public ResponseEntity<?> login(LoginUser loginUser) {
        try {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(loginUser.getUsername(), loginUser.getPassword());
            Authentication authenticate = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
            // 存入上下文
            CustomerUserDetails userDetails = (CustomerUserDetails) authenticate.getPrincipal();
            LoginUser user = userDetails.getLoginUser();
            user.setPassword(loginUser.getPassword());
            user.setPermissions(userDetails.getPermissions());
            String token = jwtUtil.createToken(user);
            return new ResponseEntity<>(token,HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.UNAUTHORIZED);
        }
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<?> register(LoginUser loginUser) {
        ResponseEntity<?> responseEntity = new ResponseEntity<>(HttpStatus.OK);
        try {
            OauthAnalysisUser oauthAnalysisUser = new OauthAnalysisUser();
            oauthAnalysisUser.setUserName(loginUser.getUsername());
            oauthAnalysisUser.setEmail(loginUser.getEmail());
            oauthAnalysisUser.setPassWord(customerSecurityConfig.passwordEncoder().encode(loginUser.getPassword()));
            oauthAnalysisUser.setAdminFlag("N");
            oauthAnalysisUserMapper.insert(oauthAnalysisUser);

            LambdaQueryWrapper<OauthAnalysisUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            LambdaQueryWrapper<OauthAnalysisUser> wrapper = lambdaQueryWrapper.eq(OauthAnalysisUser::getUserName, loginUser.getUsername());
            OauthAnalysisUser oauthUser = oauthAnalysisUserMapper.selectOne(wrapper);
            // 初始化角色未普通用户
            oauthAnalysisUserMapper.initUserRole(oauthUser.getUserId());
            String body ="注册成功";
            responseEntity = new ResponseEntity<>(body,HttpStatus.OK);
    }catch (Exception e){
        String body ="用户名或邮箱已被占用";
        responseEntity = new ResponseEntity<>(body,HttpStatus.SERVICE_UNAVAILABLE);
    }
    return responseEntity;

    }
}
