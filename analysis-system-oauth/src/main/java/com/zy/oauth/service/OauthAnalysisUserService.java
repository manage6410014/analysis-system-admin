package com.zy.oauth.service;


import com.zy.oauth.dto.LoginUser;
import org.springframework.http.ResponseEntity;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author zy
 * @since 2023-04-13
 */
public interface OauthAnalysisUserService  {

    ResponseEntity<?> login(LoginUser loginUser);

    ResponseEntity<?> register(LoginUser loginUser);
}
