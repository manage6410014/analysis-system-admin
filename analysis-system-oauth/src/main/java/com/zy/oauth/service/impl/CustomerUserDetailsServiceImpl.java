package com.zy.oauth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.zy.oauth.dto.CustomerUserDetails;
import com.zy.oauth.dto.LoginUser;
import com.zy.oauth.entity.OauthAnalysisRole;
import com.zy.oauth.entity.OauthAnalysisUser;
import com.zy.oauth.mapper.OauthAnalysisUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 自定义验证：UserDetailsService
 */
@Service
public class CustomerUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private OauthAnalysisUserMapper oauthAnalysisUserMapper;


    @Override
    public UserDetails loadUserByUsername(String username){
        //1、根据用户名查询用户信息
        LoginUser user = oauthAnalysisUserMapper.queryLoginUser(username);
        //如果查询不到数据就通过抛出异常来给出提示
        if(Objects.isNull(user)){
            throw new RuntimeException("用户名或密码错误");
        }
        // 2、查询角色及权限:
        List<String> authoritiesList = new ArrayList<>();
        List<String> authoritiesPerms = oauthAnalysisUserMapper.queryPermission(username);
        List<OauthAnalysisRole>  roleList = oauthAnalysisUserMapper.queryRole(username);
        // 若 数据库代表角色的code没有加 ROLE_ 作为前缀、需要手动加一下；
        List<String> authoritiesRoles = this.addPrefix(roleList);

        authoritiesList.addAll(authoritiesPerms);
        authoritiesList.addAll(authoritiesRoles);
        return new CustomerUserDetails(user,authoritiesList);
    }

    /**
     * 给role_code 加上前缀
     * @param roleList
     * @return
     */
    private List<String> addPrefix(List<OauthAnalysisRole> roleList) {
        StringBuilder prefix = new StringBuilder("ROLE_");
        roleList.forEach(role-> role.setRoleCode(prefix.append(role.getRoleCode()).toString()));
        return roleList.stream().map(OauthAnalysisRole::getRoleCode).collect(Collectors.toList());
    }
}

