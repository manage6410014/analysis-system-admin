package com.zy.oauth.mapper;

import com.zy.oauth.dto.LoginUser;
import com.zy.oauth.entity.OauthAnalysisRole;
import com.zy.oauth.entity.OauthAnalysisUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author zy
 * @since 2023-04-13
 */
@Mapper
public interface OauthAnalysisUserMapper extends BaseMapper<OauthAnalysisUser> {



    LoginUser queryLoginUser(@Param("username") String username);

    List<String> queryPermission(@Param("username") String username);

    List<OauthAnalysisRole> queryRole(@Param("username") String username);

    int initUserRole(@Param("userId") Integer userId);
}
